# Download

 - [1.0.0.2](release/refreshDirectory.exe)

# refresh Directory

refresh Directory는 컴파일된 파일의 아이콘이 변경되었음에도, 탐색기에 반영되지 않았을 경우, 이를 업데이트 해주는 도구 입니다.

1. __반영전__ Context Menu에서 `reset view`를 클릭<br>![01](designRes/step_01.png)
2. __업데이트 후__<br>![02](designRes/step_02.png)